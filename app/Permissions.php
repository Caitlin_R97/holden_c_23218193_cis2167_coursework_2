<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
  public function permission_role()
  {
    //this defines the one to many relationship between permissions and roles through a link table
    return $this->hasMany('App\Permissions_Roles');
  }
}
