<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
  public function permission()
  {
   return $this->hasMany('App\Permissions_Roles');
  //A link table has been placed between permissions and roles, therefore providing the one to many relationship needed
}

public function user()
{
 return $this->hasMany('App\Role_User');
 //This links to the link table with a one to many relationship
}
}
