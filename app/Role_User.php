<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_User extends Model
{
    public function roles (){
      return $this->belongsTo('App\Roles');
      //this is a link table that has a one to many relationship with the roles table
      //Many users will be assigned a role
    }

    public function users (){
      return $this->belongsTo('App\Users');
      //the link table role_user links to users, this is linked with a one to many relationship
    }

}
