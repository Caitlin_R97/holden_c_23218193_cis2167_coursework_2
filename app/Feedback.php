<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  //this is a list of the fields in the tables
  protected $table = 'feedback';
  protected $fillable = [
       'name',
       'age',
       'household_size',
       'Household_income',
       'profession',
       'education_level',
       'home_location'

   ];
}
