<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
//this defines the one to many relationship between questions and answers through a link table
public function questions(){
  return $this->belongsTo('App\questions');
}

}
