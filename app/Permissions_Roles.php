<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissions_Roles extends Model
{
  //this defines the one to many relationship between permissions and roles through a link table
    public function roles ()
    {
      return $this->belongsTo('App\Roles');
    }

    public function permissions ()
    {
      return $this->belongsTo('App\Permissions');
    }
}
