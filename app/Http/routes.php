<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//the routes here are calling the specified controllers, and can be accessed without logging in
Route::resource('/', 'HomeController');
Route::resource('/home', 'HomeController');



Route::group(['middle' => ['web']], function () {
  Route::auth();
  //the routes below this point are Authenticatable and therefore require the user to log in to view them.
  //this calls the show method in the QuestionController
  Route::get('/questions/show/{id}', 'QuestionController@show');
  //this calls the edit method in the QuestionController
  Route::get('/questions/edit/{id}', 'QuestionController@show');
  //the routes below call individual controllers within the authenticated areas.
  Route::resource('/admin', 'AdminController');
  Route::resource('/questionnaire', 'QuestionnaireController');
  Route::resource('/users', 'UsersController');
  Route::resource('/questions', 'QuestionController');
  Route::resource('/feedback', 'FeedbackController');

});
