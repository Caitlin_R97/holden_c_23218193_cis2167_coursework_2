<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Questionnaires;
use App\Questions;
use App\Answers;

class QuestionnaireController extends Controller
{
  /*
 * Secure the set of pages to the admin.
 */
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questionnaires = Questionnaires::all();

      return view('admin.questionnaires.show', ['questionnaire' => $questionnaires]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
       {
         $questionnaires = Questionnaires::lists('name', 'detail');

           // now we can return the data with the view

         return view('admin.questionnaires.create')->with('questionnaire',$questionnaires);
         // compact('feedback'));
     }
   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'required|min:3|max:255',
          'detail' => 'required'
      ]);
      $input = $request->all();

      Questionnaires::create($input);
      $questionnaires = Questionnaires::all();

      return view('/admin/questions/create', ['id' => count($questionnaires)]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {

       $questionnaires = Questionnaires::where('id',$id)->first();

       // if questionnaire does not exist return to list
       if(!$questionnaires)
       {
           return redirect('/questionnaire'); // you could add on here the flash messaging of questionnaire does not exist.
       }
       return view('/admin/questionnaires/show')->withQuestionnaires($questionnaires);
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $questionnaires = Questionnaires::findOrFail($id);

      return view('admin.questionnaires.edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $questionnaires = Questionnaires::findOrFail($id);

      $questionnaires->update($request->all());

      return redirect('questionnaire');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $questionnaires = Questionnaires::find($id);

      $questionnaires->delete();

      return redirect('questionnaire');
    }
    # view survey publicly and complete survey
      public function view_questionnaire(questionnaires $questionnaires)
      {
        // $questionnaires->option_name = unserialize($questionnaires->option_name);
        return view('admin.questionnaires.view', compact('questionnaire'));
      }

}
