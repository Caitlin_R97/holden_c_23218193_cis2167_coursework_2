<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Questionnaires;
use App\Answers;
class AnswersController extends Controller
{
  public function __construct()
{
  $this->middleware('auth');
}

public function store(Request $request, Questionnaire $questionnaire)
{
  // remove the token
  $arr = $request->except('_token');
  foreach ($arr as $key => $value) {
    $newAnswer = new Answer();
    if (! is_array( $value )) {
      $newValue = $value['answer'];
    } else {
      $newValue = json_encode($value['answer']);
    }
    $newAnswers->answer = $newValue;
    $newAnswers->question_id = $key;
    $newAnswers->user_id = Auth::id();
    $newAnswers->survey_id = $questionnaire->id;

    $newAnswer->save();
  };
  return redirect()->action('QuestionnaireController@show', [$questionnaire->id]);
}
}
