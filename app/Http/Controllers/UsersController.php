<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;
class UsersController extends Controller
{
  /*
 * Secure the set of pages to the admin.
 */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::all();

    return view('admin.users.show', ['users' => $users]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function create()
   {
     {
       $users = User::lists('name', 'email', 'password');

         // now we can return the data with the view

       return view('admin.users.create')->with('users',$users);
       // compact('feedback'));
   }
 }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
        'name' => 'required|min:3|max:255',
        'email' => 'required',
        'password' => 'required'
    ]);
    $input = $request->all();

    User::create($input);

    return redirect('/users');
    $users = User::create($request-> all());
    return redirect('/users');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $users = User::where('id',$id)->first();

    // if user does not exist return to list
    if(!$users)
    {
        return redirect('/users'); // you could add on here the flash messaging of user does not exist.
    }
    return view('/admin/users/show')->withusers($users);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $users = User::findOrFail($id);

    return view('admin.users.edit', compact('Users'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $users = User::findOrFail($id);

    $users->update($request->all());

    return redirect('users');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $users = User::find($id);

    $users->delete();

    return redirect('users');
  }
}
