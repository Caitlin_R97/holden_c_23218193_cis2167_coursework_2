<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;
use App\Questions;
use App\Questionnaires;

class QuestionController extends Controller
{
  /*
 * Secure the set of pages to the admin.
 */

  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questions = Questions::all();

      return view('admin.questions.show', ['question' => $questions]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
       {
         $questions = Questions::lists('question', 'questionnaire_id');

           // now we can return the data with the view

         return view('admin.questions.create')->with('questions',$questions);
         // compact('feedback'));
     }
   }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $input = $request->all();
      $question = Questions::create($input);
      $question->save();

      return redirect('/questionnaire');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $questions = Questions::where('id',$id)->first();

      // if questions does not exist return to list
      if(!$questions)
      {
          return redirect('/questions'); // you could add on here the flash messaging of questions does not exist.
      }
      return view('/admin/questions/show')->withQuestions($questions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
       $feedback = Questions::findOrFail($id);

       return view('admin.questions.edit', compact('questions'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $questions = Questions::findOrFail($id);

      $questions->update($request->all());

      return redirect('questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $questions = Questions::find($id);

      $questions->delete();

      return redirect('questions');
    }
}
