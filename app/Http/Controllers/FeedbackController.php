<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Feedback;
// use App\Questions;

class FeedbackController extends Controller
{
  /*
* Secure the set of pages to the admin.
*/
    public function __construct()
    {
      $this->middleware('auth');
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $feedback = Feedback::all();

      return view('admin.feedback.show', ['feedback' => $feedback]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      {
        $feedback = Feedback::lists('name', 'age', 'household_size', 'Household_income', 'profession', 'education_level', 'home_');

          // now we can return the data with the view

        return view('admin.feedback.create')->with('feedback',$feedback);

    }
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'required|min:3|max:255',
          'age' => 'required',
          'household_size' => 'required',
          'Household_income' => 'required',
          'profession' => 'required',
          'education_level' => 'required',
          'home_location' => 'required'
      ]);
      $input = $request->all();

      Feedback::create($input);

      return redirect('/feedback');
      $feedback = Feedback::create($request-> all());
      return redirect('/feedback');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $feedback = Feedback::where('id',$id)->first();

      // if feedback does not exist return to list
      if(!$feedback)
      {
          return redirect('/feedback'); // you could add on here the flash messaging of feedback does not exist.
      }
      return view('/admin/feedback/show')->withFeedback($feedback);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $feedback = Feedback::findOrFail($id);

      return view('admin.feedback.edit', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $feedback = Feedback::findOrFail($id);

      $feedback->update($request->all());

      return redirect('feedback');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $feedback = Feedback::find($id);

      $feedback->delete();

      return redirect('feedback');
    }
}
