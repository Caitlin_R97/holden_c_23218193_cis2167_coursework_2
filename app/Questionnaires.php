<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaires extends Model
{
  protected $table = 'questionnaires';
  protected $fillable = [
       'name',
       'detail'


   ];
   public function users ()
   {
     return $this->belongsTo('App\Users');
   }

   public function questions ()
   {
     return $this->hasMany('App\Questions');
   }

}
