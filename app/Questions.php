<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
  //this lists the fields in the questions table
  protected $fillable = [
       'question',
       'question2',
       'question3',
       'question4',
       'question5',
       'questionnaire_id'
   ];
   //this defines the one to many relationship between questions and questionnaires through a link table
   public function questionnaire(){
     return $this->belongsTo('App\Questionnaires');
   }

   public function user(){
     return $this->belongsTo('App\User');
     //A question can be answered by many users
     //this defines the relationship between questions and users
   }

   public function questions_answers(){
     return $this->hasMany('App\Answers');
     //this is the link table between questions and answers, defining a one to many relationship
   }
}
