<nav class="navbar navbar-inverse navbar-fixed-top">
     <div class="container-fluid">
          <ul class="nav navbar-nav">
             <a class="navbar-brand" href="/login">Admin!</a>
             <li class="active"><a href="/">Home</a></li>
             <li><a href="/feedback">Feedback</a></li>
             <li><a href="/questionnaire">Questionnaires</a></li>
             <li><a href="/users">Users</a></li>

         </ul>
     </div>
 </nav>
