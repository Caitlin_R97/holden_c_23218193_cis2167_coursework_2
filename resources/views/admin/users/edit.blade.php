@extends('layouts.app')

@section('title', 'Edit')

@section('content')
  <h1>Edit</h1>
<!-- validation errors -->
@include ('errors/errorlist')
<!-- form goes here -->
<!-- @include('partials/editquestion') -->

@endsection
