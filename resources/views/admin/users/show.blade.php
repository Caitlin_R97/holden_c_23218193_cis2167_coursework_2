@extends('layouts.app')

@section('title', 'Users')

@section('content')
  <h1>Users</h1>
@include('partials/usersinfo')

@endsection
