@extends('layouts.app')

@section('title', 'My Home Page')

@section('content')
  <h1>Edit</h1>
<!-- validation errors -->
@include ('errors/errorlist')
<!-- form goes here -->
@include('partials/editfeedback')

@endsection
