@extends('layouts.app')

@section('title', 'My Home Page')

@section('content')
        <h1>Feedback form</h1>
@include ('errors/errorlist')
<!-- form goes here -->
@include('partials/feedbackform')
@endsection
