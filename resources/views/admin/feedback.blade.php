@extends('layouts.master')

@section('title', 'Feedback')

@section('content')
  <h1>Feedback</h1>
  <section>
    @if (isset ($feedback))
      <table class="table table-striped table-bordered">
          <thead>
              <tr>
                  <td>Name</td>
                  <td>Age</td>
                  <td>Household Size</td>
                  <td>Household Income</td>
                  <td>Profession</td>
                  <td>Education Level</td>
                  <td>Home Location</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($feedback as $feedback)
                   <tr>
                    <td>{{ $feedback->name }}</td>
                    <td>{{ $feedback->age}}</td>
                    <td>{{ $feedback->household_size}}</td>
                    <td>{{ $feedback->Household_income}}</td>
                    <td>{{ $feedback->profession}}</td>
                    <td>{{ $feedback->education_level}}</td>
                    <td>{{ $feedback->home_location}}</td>
                    </tr>

                @endforeach
             </tbody>
          </table>
        @else
          <p> No feedback added yet </p>
        @endif
    </section>
@endsection
