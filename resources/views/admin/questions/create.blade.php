@extends('layouts.app')

@section('title', 'New Question')

@section('content')
        <h1>Question form</h1>
@include ('errors/errorlist')
<!-- form goes here -->
@include('partials/newquestion')
@endsection
