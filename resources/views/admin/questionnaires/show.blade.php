@extends('layouts.app')

@section('title', 'Questionnaires')

@section('content')
  <h1>Questionnaires</h1>
@include('partials/showquestionnairesform')

@endsection
