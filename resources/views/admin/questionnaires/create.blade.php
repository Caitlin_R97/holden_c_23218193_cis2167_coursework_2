@extends('layouts.app')

@section('title', 'New Questionnaire')

@section('content')
        <h1>Questionnaire form</h1>
@include ('errors/errorlist')
<!-- form goes here -->
@include('partials/questionnaireform')
@endsection
