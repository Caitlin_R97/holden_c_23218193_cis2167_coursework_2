@extends('layouts.app2')

@section('title', 'My Home Page')

@section('content')
        <h1> Welcome</h1>
        <h3>To create a new questionnaire, please log in or sign up </h3>
        <h2>Please enter your answer below...</h2>
@include ('errors/errorlist')
<!-- form goes here -->
@include('partials/feedbackform')
@endsection
