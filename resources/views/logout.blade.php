@extends('layouts.app2')

@section('title', 'Feedback')

@section('content')
  <h1>Feedback</h1>
@include('partials/feedbackinfo')
@endsection
