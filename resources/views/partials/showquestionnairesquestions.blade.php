<div class="col-md-6">
    <a href="questions/create" class="btn btn-lg btn-success pull-right">Questions</a>
</div>
     @if (isset ($questions))

         <table class="table table-striped table-bordered">
             <thead>
             <tr>
               <td>Questionnaires</td>

           </tr>
         </thead>
         <tbody>
           @foreach ($questions as $questions)

                <tr>
                  <td>{{ $questions->question}}</td>

                 <td> <a href="questions/{{ $questions->id }}/edit" class="btn btn-warning">Update</a></td>
                 <td>{!! Form::open(['method' => 'DELETE', 'route' => ['questionnaire.destroy', $questions->id]]) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                      {!! Form::close() !!}
                    </td>
                 </tr>

             @endforeach
             </tbody>
         </table>


     @else
         <p> No Questions added yet </p>
     @endif
