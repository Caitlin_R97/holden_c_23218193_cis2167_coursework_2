
    {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
        <!-- {{ csrf_token() }} !!} -->

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('detail', 'Detail:') !!}
            {!! Form::text('detail', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit questionaire', ['class' => 'btn btn-primary form-control']) !!}
        </div>


        {!! Form::close() !!}
