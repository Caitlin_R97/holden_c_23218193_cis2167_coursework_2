
    {!! Form::open(array('action' => 'FeedbackController@store', 'id' => 'createfeedback')) !!}
        <!-- {{ csrf_token() }} !!} -->

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('age', 'Age:') !!}
            {!! Form::text('age', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('household_size', 'Household size:') !!}
            {!! Form::text('household_size', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Household_income', 'Household Income:') !!}
            {!! Form::text('Household_income', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('profession', 'profession:') !!}
            {!! Form::text('profession', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('education_level', 'Education Level:') !!}
            {!! Form::text('education_level', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('home_location', 'Where do you live?:') !!}
            {!! Form::text('home_location', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit questionaire', ['class' => 'btn btn-primary form-control']) !!}
        </div>


        {!! Form::close() !!}
