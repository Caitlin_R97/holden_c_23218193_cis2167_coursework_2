@if (isset ($questionnaires))
  <table class="table table-striped table-bordered">
      <thead>
          <tr>
              <td>Name</td>
              <td>Detail</td>

          </tr>
        </thead>
        <tbody>
          @foreach ($questionnaires as $questionnaires)
               <tr>
                <td>{{ $questionnaires->name }}</td>
                <td>{{ $questionnaires->detail}}</td>

                </tr>

            @endforeach
         </tbody>
      </table>
    @else
      <p> No questionnaires added yet </p>
    @endif
