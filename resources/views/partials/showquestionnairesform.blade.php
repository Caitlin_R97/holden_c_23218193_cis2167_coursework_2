<div class="col-md-6">
    <a href="questionnaire/create" class="btn btn-lg btn-success pull-right">New Questionnaire</a>
</div>
     @if (isset ($questionnaire))

         <table class="table table-striped table-bordered">
             <thead>
             <tr>
               <td>Questionnaires</td>

           </tr>
         </thead>
         <tbody>
           @foreach ($questionnaire as $questionnaires)

                <tr>
                 <td><a href="/questions/show/{{$questionnaires->id}}"</a>{{$questionnaires->id}}</td>
                  <td>{{ $questionnaires->detail}}</td>

                 
                 <td>{!! Form::open(['method' => 'DELETE', 'route' => ['questionnaire.destroy', $questionnaires->id]]) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                      {!! Form::close() !!}
                    </td>
                 </tr>

             @endforeach
             </tbody>
         </table>


     @else
         <p> No Questionnaires added yet </p>
     @endif
