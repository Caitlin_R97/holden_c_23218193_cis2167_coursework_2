{!! Form::model($questions, ['method' => 'PATCH', 'url' => 'questions/' . $questions->id]) !!}

<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('questionnaire_id', $id) !!}
</div>
<div class="form-group">
    {!! Form::label('question2', 'Question 2:') !!}
    {!! Form::text('question2', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('questionnaire_id', $id) !!}
</div>
<div class="form-group">
    {!! Form::label('question3', 'Question 3:') !!}
    {!! Form::text('question3', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('questionnaire_id', $id) !!}
</div>
<div class="form-group">
    {!! Form::label('question4', 'Question 4:') !!}
    {!! Form::text('question4', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('questionnaire_id', $id) !!}
</div>
<div class="form-group">
    {!! Form::label('question5', 'Question 5:') !!}
    {!! Form::text('question5', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('questionnaire_id', $id) !!}
</div>



<div class="form-group">
    {!! Form::submit('Update question', ['class' => 'btn btn-primary form-control']) !!}
</div>


{!! Form::close() !!}
