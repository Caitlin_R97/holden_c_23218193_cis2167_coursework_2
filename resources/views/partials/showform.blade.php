<div class="col-md-6">
    <a href="feedback/create" class="btn btn-lg btn-success pull-right">New Feedback</a>
</div>
     @if (isset ($feedback))

         <table class="table table-striped table-bordered">
             <thead>
             <tr>
               <td>Name</td>
               <td>Age</td>
               <td>Household Size</td>
               <td>Household Income</td>
               <td>Profession</td>
               <td>Education Level</td>
               <td>Home Location</td>
           </tr>
         </thead>
         <tbody>
           @foreach ($feedback as $feedback)
                <tr>
                 <td>{{ $feedback->name }}</td>
                 <td>{{ $feedback->age}}</td>
                 <td>{{ $feedback->household_size}}</td>
                 <td>{{ $feedback->Household_income}}</td>
                 <td>{{ $feedback->profession}}</td>
                 <td>{{ $feedback->education_level}}</td>
                 <td>{{ $feedback->home_location}}</td>
                 <td> <a href="feedback/{{ $feedback->id }}/edit" class="btn btn-warning">Update</a></td>
                 <td>{!! Form::open(['method' => 'DELETE', 'route' => ['feedback.destroy', $feedback->id]]) !!}
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                      {!! Form::close() !!}
                    </td>`
                 </tr>

             @endforeach
             </tbody>
         </table>
     @else
         <p> No feedback added yet </p>
     @endif
