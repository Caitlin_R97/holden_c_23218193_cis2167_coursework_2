{!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'questionnaire/' . $questionnaire->id]) !!}
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('detail', 'Detail:') !!}
    {!! Form::text('detail', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::submit('Update questionnaire', ['class' => 'btn btn-primary form-control']) !!}
</div>


{!! Form::close() !!}
