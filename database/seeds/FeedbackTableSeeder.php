<?php

use Illuminate\Database\Seeder;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('feedback')->insert([
           ['id' => 1, 'name' => "Caitlin", 'age' => "20", 'household_size' => '3', 'household_income' => "20,000", 'profession' => "student", 'education_level' => "Undergraduate", 'home_location' => 'Brighton' ],
       ]);
    }
}
