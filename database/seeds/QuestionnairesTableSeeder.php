<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         // add a known user

         DB::table('questionnaires')->insert([
             ['id' => 1, 'name' => "Pets",
                 'detail' => 'what kind of pets a person has',
                 
             ],
         ]);

     }
}
