<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //this creates the fields in the tables and asigns their data type
      Schema::create('feedback', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('age');
          $table->integer('household_size');
          $table->integer('Household_income');
          $table->string('profession');
          $table->string('education_level');
          $table->string('home_location');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feedback');
    }
}
