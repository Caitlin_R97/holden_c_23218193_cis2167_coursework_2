<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //this creates the fields in the tables and asigns their data type
      Schema::create('answers', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->integer('question_id');
          $table->integer('questionnaire_id');
          $table->string('answer');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
