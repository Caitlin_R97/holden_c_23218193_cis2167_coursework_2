<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //this creates the fields in the tables and asigns their data type
      Schema::create('roles', function (Blueprint $table) {
          $table->increments('id');
          $table->string('role_name');
          $table->string('detail');
          $table->integer('permission_id')->unsigned();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
