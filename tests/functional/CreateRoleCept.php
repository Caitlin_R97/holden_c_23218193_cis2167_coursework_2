<?php
  $I = new FunctionalTester($scenario);

  $I->am('admin');
  $I->wantTo('create a new role');
  Auth::loginUsingId(1);
  // When
  $I->amOnPage('/admin/role/index');
  $I->see('All Roles', 'h1');
  $I->dontSee('Randomtest');
  // And
  $I->click('Create Role');

  // Then
  $I->amOnPage('/admin/role/create');
  // And
  $I->see('Add Role', 'h1');
  $I->submitForm('#createrole', [
     'title' => 'Randomtest',
 ]);
  // Then
  $I->seeCurrentUrlEquals('/admin/role');
 $I->see('All Roles', 'h1');
 //$I->see('New role added!');
 //$I->see('Randomtest');
