<?php
// Given
$I= new FunctionalTester($scenario);
$I->amOnPage('/dashboard');
//  And
$I->click('Add new blog post');
// Then
$I->seeCurrentUrlEquals('/author/blog');
$I->see('Add new blog', 'h1');

// When
$I->fillField('blog name', 'my blog');
// and
$I->fillField('blog about', 'me');
// and
// Then
$I->seeCurrentUrlMatches('~/admin/blog/(\d+)~');
$I->see('new blog', 'h1');
