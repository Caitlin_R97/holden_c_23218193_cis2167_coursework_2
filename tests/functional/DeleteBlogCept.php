<?php
// Given
$I= new FunctionalTester($scenario);
$I->amOnPage('/dash');
//  And
$I->click('delete category');
// Then
$I->seeCurrentUrlEquals('/author/category');

// When
$I->fillField('category name', 'makeup');
// and
$I->fillField('category about', 'makeup items');
// and
// Then
$I->seeCurrentUrlMatches('~/author/category/(\d+)~');
$I->see('category deleted', 'h1');
